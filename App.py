import sys
import requests
from PyQt5.QtCore import QUrl, QSize, QModelIndex
from PyQt5.QtGui import QStandardItemModel, QStandardItem, QPixmap, QIcon
from PyQt5.QtWidgets import *
import time

import InfoTableForm
import MainWin
import InfoWin
import InfoListItem

class MainWin(QWidget, MainWin.Ui_Form):

    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.pushButton.clicked.connect(self.login)

    def login(self):
        print(self.lineEdit.text())
        print(self.lineEdit_2.text())
        headers = {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
            'Accept-Encoding': 'gzip, deflate, br',
            'Accept-Language': 'zh-CN,zh;q=0.9',
            'Cache-Control': 'no-cache',
            'Connection': 'keep-alive',
            'Cookie': '_ga=GA1.2.1758034810.1538036919; retUrlpcRegister=https%3A//www.qhfax.com/; _gid=GA1.2.416116702.1542165936; Hm_lvt_4c5947cd21b22c2401340b46429e3893=1541726893,1542002609,1542165936,1542178922; JSESSIONID=DDA13E6023893B819196A2CF6108F937; Hm_lpvt_4c5947cd21b22c2401340b46429e3893=1542249805',
            'Host': 'www.qhfax.com',
            'Pragma': 'no-cache',
            'Upgrade-Insecure-Requests': '1',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36'
        }
        result = requests.post('https://www.qhfax.com/user/login', data={'userName': self.lineEdit.text(), 'password': self.lineEdit_2.text()}, headers=headers)
        print(result.text)


class InfoListItem(QWidget, InfoListItem.Ui_Form):

    def __init__(self, index, data):
        super().__init__()
        self.setupUi(self)
        resp = requests.get(data['file_path'])
        self.img = QPixmap()
        self.img.loadFromData(resp.content)
        self.label.setPixmap(self.img)
        self.label_2.setText(index + '.' + data['title'])
        timeArray = time.localtime(data['create_time']/1000.0)  # 秒数
        otherStyleTime = time.strftime("%Y-%m-%d %H:%M:%S", timeArray)
        self.label_3.setText(otherStyleTime)

    def sizeHint(self):
        # 决定item的高度
        return QSize(self.img.width() * 3, self.img.height())


class InfoListView(QWidget, InfoWin.Ui_Form):

    def __init__(self, pageSize=10, pageNo=1):
        super().__init__()
        self.setupUi(self)
        self.pageSize = pageSize
        self.pageNo = pageNo
        self.prevPageButton.clicked.connect(self.prevPage)
        self.nextPageButton.clicked.connect(self.nextPage)
        self.loadPage()

    def loadPage(self):
        result = requests.get('https://www.qhfax.com/aboutUs/getNoticeByPageForFront?pageSize=%d&pageNo=%d' % (self.pageSize, self.pageNo))
        resultJson = result.json()
        model = QStandardItemModel()
        self.listView.setModel(model)
        for n in resultJson['result']['list']:
            item = QStandardItem()
            model.appendRow(item)
            index = model.indexFromItem(item)
            widget = InfoListItem(str(index.row() + 1), n)
            item.setSizeHint(widget.sizeHint())  # 主要是调整item的高度
            self.listView.setIndexWidget(index, widget)


    def prevPage(self):
        self.pageNo -= 1
        self.loadPage()
        if self.pageNo <= 0:
            self.pageNo = 1

    def nextPage(self):
        self.pageNo += 1
        self.loadPage()


class AbstractInfoTableView(QWidget, InfoTableForm.Ui_Form):

    def __init__(self, pageSize=10, pageNo=1):
        super().__init__()
        self.setupUi(self)
        self.pageSize = pageSize
        self.pageNo = pageNo
        self.prevPageButton.clicked.connect(self.prevPage)
        self.nextPageButton.clicked.connect(self.nextPage)

    def loadPage(self):
        pass

    def prevPage(self):
        self.pageNo -= 1
        self.loadPage()
        if self.pageNo <= 0:
            self.pageNo = 1

    def nextPage(self):
        self.pageNo += 1
        self.loadPage()


class InfoTableView(AbstractInfoTableView):

    def loadPage(self):
        try:
            result = requests.get('https://www.qhfax.com/aboutUs/getNoticeByPageForFront?pageSize=%d&pageNo=%d' % (self.pageSize, self.pageNo))
            resultJson = result.json()
            model = QStandardItemModel(len(resultJson['result']['list']), 4)
            self.tableView.setModel(model)
            row = 0
            for n in resultJson['result']['list']:
                timeArray = time.localtime(n['create_time'] / 1000.0)  # 秒数
                otherStyleTime = time.strftime("%Y-%m-%d %H:%M:%S", timeArray)
                resp = requests.get(n['file_path'])
                img = QPixmap()
                img.loadFromData(resp.content)
                item1 = QStandardItem(n['title'])
                item1.setIcon(QIcon(img))
                model.setItem(row, 0, item1)
                item2 = QStandardItem(n['content'])
                model.setItem(row, 1, item2)
                model.setItem(row, 2, QStandardItem(otherStyleTime))
                item4 = QStandardItem()
                model.setItem(row, 3, item4)
                index = model.index(row, 3)
                label = QLabel()
                label.setPixmap(img);
                item4.setSizeHint(QSize(img.width(), img.height()))
                self.tableView.setIndexWidget(index, label)
                row += 1
        except Exception as e:
            print(e)

if __name__ == '__main__':
    app = QApplication(sys.argv)
    win = InfoTableView()
    win.loadPage()
    win.show()
    sys.exit(app.exec_())
