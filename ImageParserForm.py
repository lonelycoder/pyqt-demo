# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ImageParserForm.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(600, 800)
        self.gridLayout = QtWidgets.QGridLayout(Form)
        self.gridLayout.setObjectName("gridLayout")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label = QtWidgets.QLabel(Form)
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        self.addrLineEdit = QtWidgets.QLineEdit(Form)
        self.addrLineEdit.setObjectName("addrLineEdit")
        self.horizontalLayout.addWidget(self.addrLineEdit)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label_2 = QtWidgets.QLabel(Form)
        self.label_2.setObjectName("label_2")
        self.horizontalLayout_2.addWidget(self.label_2)
        self.dirLineEdit = QtWidgets.QLineEdit(Form)
        self.dirLineEdit.setObjectName("dirLineEdit")
        self.horizontalLayout_2.addWidget(self.dirLineEdit)
        self.choosePushButton = QtWidgets.QPushButton(Form)
        self.choosePushButton.setObjectName("choosePushButton")
        self.horizontalLayout_2.addWidget(self.choosePushButton)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.listView = QtWidgets.QListView(Form)
        self.listView.setObjectName("listView")
        self.verticalLayout.addWidget(self.listView)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.startPushButton = QtWidgets.QPushButton(Form)
        self.startPushButton.setObjectName("startPushButton")
        self.horizontalLayout_3.addWidget(self.startPushButton)
        self.stopPushButton = QtWidgets.QPushButton(Form)
        self.stopPushButton.setObjectName("stopPushButton")
        self.horizontalLayout_3.addWidget(self.stopPushButton)
        self.verticalLayout.addLayout(self.horizontalLayout_3)
        self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 1)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "图片下载器"))
        self.label.setText(_translate("Form", "网址："))
        self.label_2.setText(_translate("Form", "目录："))
        self.choosePushButton.setText(_translate("Form", "选择"))
        self.startPushButton.setText(_translate("Form", "下载"))
        self.stopPushButton.setText(_translate("Form", "停止"))

