import matplotlib.pyplot as plt
from datetime import datetime

# 第一组数据的时间点
time1 = [
    datetime(2024, 6, 25, 10, 0),
    datetime(2024, 6, 25, 11, 0),
    datetime(2024, 6, 25, 12, 0),
    datetime(2024, 6, 25, 13, 0),
    datetime(2024, 6, 25, 14, 0)
]

# 第二组数据的时间点
time2 = [
    datetime(2024, 6, 25, 10, 5),
    datetime(2024, 6, 25, 11, 10),
    datetime(2024, 6, 25, 12, 5),
    datetime(2024, 6, 25, 13, 10),
    datetime(2024, 6, 25, 14, 2)
]

# 第三组数据的时间点
time3 = [
    datetime(2024, 6, 25, 9, 45),  # 起点稍早于第一组
    datetime(2024, 6, 25, 10, 45),
    datetime(2024, 6, 25, 11, 45),
    datetime(2024, 6, 25, 12, 45),
    datetime(2024, 6, 25, 13, 45)
]

# 第四组数据的时间点
time4 = [
    datetime(2024, 6, 25, 10, 10),  # 起点介于第一二组之间
    datetime(2024, 6, 25, 11, 20),
    datetime(2024, 6, 25, 12, 10),
    datetime(2024, 6, 25, 13, 20),
    datetime(2024, 6, 25, 14, 30)
]

# 对应的值
values1 = [2, 3, 5, 7, 11]
values2 = [1, 4, 6, 8, 10]
values3 = [3, 2, 4, 6, 8]
values4 = [5, 7, 9, 11, 13]

# 将时间转换为相对于各自起点的时间差（秒）
time1_relative = [(t - time1[0]).total_seconds() for t in time1]
time2_relative = [(t - time2[0]).total_seconds() for t in time2]
time3_relative = [(t - time3[0]).total_seconds() for t in time3]
time4_relative = [(t - time4[0]).total_seconds() for t in time4]

# 绘制四组数据的曲线图
plt.plot(time1_relative, values1, label='Data Set 1', marker='o')
plt.plot(time2_relative, values2, label='Data Set 2', linestyle='--', marker='s')
plt.plot(time3_relative, values3, label='Data Set 3', linestyle='-.', marker='^')
plt.plot(time4_relative, values4, label='Data Set 4', linestyle=':', marker='x')

# 设置图例
plt.legend()

# 设置图表标题和坐标轴标签
plt.title('Comparison of Four Data Sets Starting from Different Points')
plt.xlabel('Time (seconds)')
plt.ylabel('Value')

# 显示网格
plt.grid(True)

# 显示图表
plt.show()