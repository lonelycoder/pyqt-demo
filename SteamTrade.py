import requests
from pandas.io import json

json_trade = {
  "newversion":"true",
  "version":2,
  "me":{
    "assets":[
        {
          "appid":570,
          "contextid":"2",
          "amount":"1",
          "assetid":"18039950257"
        }
    ],
    "currency":[

    ],
    "ready":"false"
  },
  "them":{
    "assets":[

    ],
    "currency":[

    ],
    "ready":"false"
  }
}
data = {
"sessionid":"e25b36ff562f9779b4321f67",
"serverid":"1",
"partner":"76561198358277009",
"tradeoffermessage":"hello",
"json_tradeoffer":"",
"captcha":"",
"trade_offer_create_params":"",
"tradeofferid_countered":""
}
data["json_tradeoffer"] = json.dumps(json_trade)
header = {
'referer': 'https://steamcommunity.com/tradeoffer/new/?partner=1019355645&token=u3pOg9kM',
'cookie':'sessionid=e25b36ff562f9779b4321f67; steamCountry=US%7C8a28593055ed4a6af2c70caa80ee89f8; timezoneOffset=28800,0; _ga=GA1.2.777293945.1588040392; _gid=GA1.2.1834243358.1588040392; steamLoginSecure=76561198979621373%7C%7C69F1405F887C8ABFEC527300795ACCB40299AD4E; steamMachineAuth76561198979621373=21C8CD56CD5A6A08404D5E1CD61377DC1143D086; browserid=1570010957648664315'
}
proxies = {
'http':'http://127.0.0.1:1080',
'https':'http://127.0.0.1:1080'
}
result = requests.post("https://steamcommunity.com/tradeoffer/new/send", data=data, headers=header, proxies=proxies, verify=False)
print(result.text)