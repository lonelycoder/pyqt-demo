import threading
import time

import serial.tools.list_ports
from serial import Serial
running = True

def read_data(com:Serial):
    global running
    while running:
        rece_data = com.readline()
        if rece_data:
            msg = rece_data.decode('utf-8').strip()
            if msg == 'exit':
                running = False
            else:
                print(msg)
        else:
            running = False

if __name__ == '__main__':
    com = serial.Serial(port='COM1', baudrate=9600)
    if com.isOpen():
        com.write('hello,world!'.encode('utf-8'))
        t = threading.Thread(target=read_data, args=(com,))
        t.start()
        t.join()
        com.close()