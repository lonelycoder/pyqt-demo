import heapq

class Node:
    def __init__(self, i, j, x, y):
        self.i = i
        self.j = j
        self.x = x
        self.y = y
        print((i, j))
    def __lt__(self, other):
        if self.x + self.y < other.x + other.y:
            return True
        elif self.x + self.y == other.x + other.y:
            return self.x < other.x
        return False

list1 = [1, 3, 4, 5, 6]
list2 = [2, 4, 5, 6, 7]
num = 10
l1 = len(list1)
l2 = len(list2)
li = [Node(0, 0, list1[0], list2[0])]
res = []
s = set()
while li:
    n = heapq.heappop(li)
    if str(n.i) + str(n.j) not in s:
        res.append((n.x, n.y))
        s.add(str(n.i) + str(n.j))
    if len(res) >= num:
        break
    if n.i + 1 < l1:
        heapq.heappush(li, Node(n.i + 1, n.j, list1[n.i + 1], list2[n.j]))
    if n.j + 1 < l2:
        heapq.heappush(li, Node(n.i, n.j + 1, list1[n.i], list2[n.j + 1]))

print(res)